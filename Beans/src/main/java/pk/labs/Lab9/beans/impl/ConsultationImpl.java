/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.io.Serializable;
import java.util.Date;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.Term;

/**
 *
 * @author st
 */
public class ConsultationImpl implements Consultation,Serializable {
    
    String student;
    
    public String getStudent() {
        return student;
    }
    
    public void setStudent(String student) {
        this.student=student;
    }
    
    Term term;
    public Term getTerm()
    {
        return term;
    }
    public void setTerm(Term term)
    {
        this.term=term;
    }
    
    public Date getBeginDate() {
        return term.getBegin();
    }

    public Date getEndDate() {
        return term.getEnd();
    }

    public void prolong(int minutes) throws PropertyVetoException {
        if(minutes <= 0) minutes=0;  
        int oldDuration = this.term.getDuration();
        this.vcs.fireVetoableChange("Term", oldDuration, oldDuration + minutes);
        this.term.setDuration(this.term.getDuration() + minutes);
    }
    
    VetoableChangeSupport vcs = new VetoableChangeSupport(this);
     public void addVetoableChangeListener(
         VetoableChangeListener listener) {
       vcs.addVetoableChangeListener(listener);
     }

    public void removeVetoableChangeListener(
        VetoableChangeListener listener) {
      vcs.removeVetoableChangeListener(listener);
    }
    
    public ConsultationImpl()
    {
        this.student=new String();
        this.term=new TermImpl();
    }
    public ConsultationImpl(String student,Term term)
    {
        this.student=student;
        this.term=term;
    }
}
