/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;

/**
 *
 * @author Tomek
 */
public class ConsultationListFactoryImpl implements ConsultationListFactory{
    ConsultationList lista;
    public ConsultationList create() {
       lista= new ConsultationListImpl();
       return lista;
    }

    public ConsultationList create(boolean deserialize) {
        if (deserialize) {
//      try
//      {
//         FileInputStream fileIn = new FileInputStream("lista");
//         ObjectInputStream in = new ObjectInputStream(fileIn);
//         lista = (ConsultationList) in.readObject();
//         in.close();
//         fileIn.close();
//      }catch(IOException i)
//      {
//      }catch(ClassNotFoundException c)
//      {
//      }  
            try
            {
            XMLDecoder decoder = new XMLDecoder(new BufferedInputStream (new FileInputStream("lista.xml")));
               lista = (ConsultationListImpl)decoder.readObject();
            }
            catch(IOException i)
            {
            
            }
      return lista;
        }
        else
        {
            return create();
        }
    }

    public void save(ConsultationList consultationList) {
            try
            {
                XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream (new FileOutputStream("lista.xml")));
                encoder.writeObject(consultationList);
                encoder.close();
           }
            catch(IOException i)
      {
      }
//        try
//      {
//         FileOutputStream fileOut = new FileOutputStream("lista");
//         ObjectOutputStream out = new ObjectOutputStream(fileOut);
//         out.writeObject(consultationList);
//         out.flush()
//         out.close();
//         fileOut.close();
//      }catch(IOException i)
//      {
//      }
    }
    
}
