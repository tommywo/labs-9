/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;

/**
 *
 * @author st
 */
public class ConsultationListImpl implements ConsultationList, Serializable,VetoableChangeListener {
    private List<Consultation> lista=new LinkedList<Consultation>();
    private PropertyChangeSupport pcs=new PropertyChangeSupport(this);
    
    public int getSize() {
        return lista.size();
    }

    public Consultation[] getConsultation() {
        return lista.toArray(new Consultation[lista.size()]);
    }
    public void setConsultation(Consultation[] c) {
        lista=new LinkedList<Consultation>();
        for (Consultation consultation : c) {
            lista.add(consultation);
        }
               
    }
    public Consultation getConsultation(int index) {
        return lista.get(index);
    }
    
    public void addConsultation(Consultation consultation) throws PropertyVetoException {
        Consultation[] temp = getConsultation();
        if(IsColiding(consultation))
                throw new PropertyVetoException("Veto!", new PropertyChangeEvent(this, "lista", temp, temp));
        lista.add(consultation);
        ((ConsultationImpl)consultation).addVetoableChangeListener(this);
        Consultation[] temp2 = getConsultation();

        pcs.firePropertyChange("consultation", temp, temp2);
    }
    public boolean IsColiding(Consultation consultation)
    {        for(int i=0; i < this.lista.size();i++)

        {
            if((consultation.getEndDate().after(this.getConsultation(i).getBeginDate()) && consultation.getBeginDate().before(this.getConsultation(i).getBeginDate()))

                    ||(consultation.getBeginDate().before(this.getConsultation(i).getEndDate()) && consultation.getEndDate().after(this.getConsultation(i).getBeginDate())))

            {
                return true;
            }    
        }   

        return false;
    }
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
//        Consultation oldValue =(Consultation)evt.getOldValue();
//        Consultation newValue =(Consultation)evt.getNewValue();
//        lista.remove(oldValue);
//        lista.add(newValue);
//        if(IsColiding(newValue))
//            throw new PropertyVetoException("Veto", evt);
        
            
    }
}
