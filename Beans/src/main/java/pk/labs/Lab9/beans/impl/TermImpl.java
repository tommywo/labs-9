/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.Lab9.beans.impl;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author st
 */
public class TermImpl implements pk.labs.Lab9.beans.Term , Serializable {
    private Date begin;
    private int duration;

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        if (duration>0) {
        this.duration = duration;
        }
    }

    public Date getEnd() {
        return new Date(begin.getTime()+duration*60*1000);
    }
}
